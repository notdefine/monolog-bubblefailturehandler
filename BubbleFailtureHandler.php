<?php declare(strict_types=1);

namespace Mms\BaseApp\Logger\Monolog;

use Monolog\Handler\GroupHandler;

/**
 * Only bubble a log entry to the next handler when this handlers could not detach the
 * log entry. Usefull if you want to log to an api and only want to log to a filesystem
 * handler if that fails.
 *
 * @author Thomas Eimers <notdefine@gmx.de>
 */
class BubbleFailureHandler extends GroupHandler
{
    /**
     * Handles a record.
     *
     * All records may be passed to this method, and the handler should discard
     * those that it does not want to handle.
     *
     * The return value of this function controls the bubbling process of the handler stack.
     * Unless the bubbling is interrupted (by returning true), the Logger class will keep on
     * calling further handlers in the stack with a given log record.
     *
     * @param array $record The record to handle
     * @return bool  true means that this handler handled the record, and that bubbling is not permitted.
     *                      false means the record was either not processed or that this handler allows bubbling.
     */
    public function handle(array $record): bool
    {
        $bubbleUp = true;
        if ($this->processors) {
            foreach ($this->processors as $processor) {
                $record = call_user_func($processor, $record);
            }
        }

        foreach ($this->handlers as $handler) {
            try {
                $handler->handle($record);
            } catch (\Throwable $e) {
                // What failure?
                $bubbleUp = false;
            }
        }
        return $bubbleUp;
    }

    /**
     * {@inheritdoc}
     */
    public function handleBatch(array $records): void
    {
        if ($this->processors) {
            $processed = [];
            foreach ($records as $record) {
                foreach ($this->processors as $processor) {
                    $processed[] = call_user_func($processor, $record);
                }
            }
            $records = $processed;
        }

        foreach ($this->handlers as $handler) {
            try {
                $handler->handleBatch($records);
            } catch (\Throwable $e) {
                // What failure?
            }
        }
    }
}
